var fs = require('fs');

var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json({
	limit : '50mb'
})); // for parsing application/json
app.use(bodyParser.raw({
	limit : '50mb'
}));
app.use("/", express.static('public'));
app.listen(process.env.PORT || 3000);

//
app.get('/load', function(req, res) {

	fs.readFile("save.gde", 'utf8', function(err, data) {
		console.info(data);
		if (err) {
			res.send("ko");
		}
		res.send(data);
	});

});

//
app.post('/save', function(req, res) {



	fs.writeFile("save.gde", JSON.stringify(req.body), function(err) {
		if (err) {
			return console.log(err);
		}

		console.log("The file was saved!");
	});

	res.send("ok");
});