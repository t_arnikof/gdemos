// create web audio api context

function frequencyFromNoteNumber(note) {
	return 440 * Math.pow(2, (note - 69) / 12);
}

try {
	var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
	// distortion.curve = makeDistortionCurve(00);
	// create Oscillator node
	var gainNode = audioCtx.createGain();
	gainNode.connect(audioCtx.destination);

} catch (e) {
	alert(e);
};

var oscillator=null;
var playOne= function(dot) {
	
	if (!isOscillator) return;
	if (oscillator==null) 
	var oscillator = audioCtx.createOscillator();
	oscillator.connect(gainNode);
	oscillator.type = 'triangle';
	oscillator.onended = function(ev) {
		oscillator.disconnect();
	};
	oscillator.frequency.value = frequencyFromNoteNumber(dot); // value
	// in
	// hertz
	oscillator.start(0);
	oscillator.stop(audioCtx.currentTime + 0.1);
};

// https://developer.mozilla.org/en-US/docs/Web/API/OscillatorNode/setPeriodicWave
var playFrequence = function(frequences, number) {

	try {
		if (!number) {
			number = 0;
		} else if (number >= frequences.length) {
			return;
		}

		var frequence = frequences[number].n;

		var oscillator = audioCtx.createOscillator();
		oscillator.connect(gainNode);
		oscillator.type = 'triangle';
		oscillator.onended = function(ev) {
			oscillator.disconnect();

			playFrequence(frequences, number + 1);
		};
		oscillator.frequency.value = frequencyFromNoteNumber(frequence); // value
	
		// in
		// hertz
		oscillator.start(0);
		oscillator.stop(audioCtx.currentTime + 0.11);

	} catch (e) {
		log(e);
	}
};


//https://sonoport.github.io/synthesising-sounds-webaudio.html
	
function kick() {
	var audioContext = new (window.AudioContext || window.webkitAudioContext)();
	
    var osc = audioContext.createOscillator();
    var osc2 = audioContext.createOscillator();
    var gainOsc = audioContext.createGain();
    var gainOsc2 = audioContext.createGain();

    osc.type = "triangle";
    osc2.type = "sine";

    gainOsc.gain.setValueAtTime(1, audioContext.currentTime);
    gainOsc.gain.exponentialRampToValueAtTime(0.001, audioContext.currentTime + 0.5);

    gainOsc2.gain.setValueAtTime(1, audioContext.currentTime);
    gainOsc2.gain.exponentialRampToValueAtTime(0.001, audioContext.currentTime + 0.5);

    osc.frequency.setValueAtTime(120, audioContext.currentTime);
    osc.frequency.exponentialRampToValueAtTime(0.001, audioContext.currentTime + 0.5);

    osc2.frequency.setValueAtTime(50, audioContext.currentTime);
    osc2.frequency.exponentialRampToValueAtTime(0.001, audioContext.currentTime + 0.5);

    osc.connect(gainOsc);
    osc2.connect(gainOsc2);
    gainOsc.connect(audioContext.destination);
    gainOsc2.connect(audioContext.destination);

    osc.start(audioContext.currentTime);
    osc2.start(audioContext.currentTime);

    osc.stop(audioContext.currentTime + 0.5);
    osc2.stop(audioContext.currentTime + 0.5);

};

function noise() {
	var audioContext = new (window.AudioContext || window.webkitAudioContext)();
    var node = audioContext.createBufferSource(),
        buffer = audioContext.createBuffer(1, 4096, audioContext.sampleRate),
        data = buffer.getChannelData(0);

    for (var i = 0; i < 4096; i++) {

        data[i] = Math.random();
    }

    node.buffer = buffer;
    node.loop = true;
    node.start(audioContext.currentTime);
    node.stop(audioContext.currentTime + 0.2);
    node.connect(audioContext.destination);
};

//kick();

// https://dev.opera.com/articles/drum-sounds-webaudio/
function snare() {
	var audioContext = new (window.AudioContext || window.webkitAudioContext)();
var filter = audioContext.createBiquadFilter();
filter.type = "highpass";

filter.frequency.setValueAtTime(100, audioContext.currentTime);
filter.frequency.linearRampToValueAtTime(1000, audioContext.currentTime + 0.2);



var osc3 = audioContext.createOscillator();
var gainOsc3 = audioContext.createGain();

osc3.type = "triangle";
osc3.frequency.value = 100;

gainOsc3.gain.setValueAtTime(0, audioContext.currentTime);
gainOsc3.gain.exponentialRampToValueAtTime(0.01, audioContext.currentTime + 0.1);
}
snare();

