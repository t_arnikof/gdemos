//http://cancerbero.mbarreneche.com/raphaeltut/
//http://code.tutsplus.com/tutorials/an-introduction-to-the-raphael-js-library--net-7186 
// https://en.wikipedia.org/wiki/List_of_musical_symbols
// http://readysetraphael.com/
var hoverElement=null;

var options = {
	x : 0,
	y : 50,
	space : 10,
	line : 5,
	width : 32000,
	height : 300,
	start:100
};

var svg= {
		
crotchet:	'<path xmlns="http://www.w3.org/2000/svg" d="m 153.03812,108.7706 a 11.237947,7.3223042 0 1 1 -0.33175,-1.76601" transform="matrix(1.2477873,-0.63522684,0.59622185,1.3694102,-142.33379,929.29558)" id="path3827" style="fill-opacity:1;stroke:#000000;stroke-width:0;stroke-miterlimit:4;stroke-opacity:0.26666667;stroke-dasharray:none"/>'	
		
};

var symbols = {
	treble : 'M 249.285 163.66 c 6.249 -0.554 12.196 1.929 15.953 6.996 c 3.612 4.871 4.82 11.305 3.697 17.22 c -1.161 6.112 -5.056 11.215 -10.215 14.57 c -1.295 0.842 -2.659 1.577 -4.066 2.213 c 0.285 2.239 0.568 4.479 0.854 6.718 c 0.528 4.152 1.493 8.49 1.466 12.679 c -0.035 5.692 -1.895 11.404 -6.922 14.574 c -4.966 3.131 -11.522 3.213 -16.473 0.059 c -5.063 -3.225 -8.647 -9.761 -6.17 -15.689 c 2.471 -5.908 10.096 -7.787 14.114 -2.265 c 2.261 3.105 2.512 7.848 -0.21 10.77 c -1.623 1.741 -4.773 3.128 -7.147 2.193 c 0.065 3.102 4.807 4.653 7.333 4.56 c 3.651 -0.158 7.244 -1.458 9.605 -4.348 c 2.462 -3.013 3.169 -6.967 3.072 -10.758 c -0.074 -2.892 -0.647 -5.776 -1.053 -8.636 c -0.413 -2.917 -0.826 -5.833 -1.238 -8.75 c -13.134 3.177 -25.259 -4.743 -30.495 -16.646 c -2.559 -5.812 -3.601 -12.502 -2.517 -18.785 c 1.101 -6.383 4.383 -12.121 8.371 -17.136 c 4.075 -5.124 8.888 -9.597 13.69 -14.021 c 0.552 -0.508 1.104 -1.016 1.654 -1.525 c 0.451 -0.415 0.567 -0.357 0.458 -1.014 c -0.083 -0.51 -0.166 -1.02 -0.243 -1.53 c -0.332 -2.184 -0.596 -4.378 -0.771 -6.579 c -0.309 -3.859 -0.404 -7.794 -0.026 -11.652 c 0.504 -5.149 1.931 -10.345 4.083 -15.045 c 1.074 -2.347 2.381 -4.647 4.131 -6.56 c 1.235 -1.35 3.232 -3.132 5.184 -3.223 c 1.243 -0.058 2.177 1.656 2.682 2.551 c 1.141 2.025 1.925 4.258 2.604 6.472 c 1.585 5.18 2.723 10.762 2.593 16.201 c -0.159 6.744 -2.421 13.496 -5.766 19.307 c -1.759 3.054 -3.859 5.908 -6.23 8.514 c -1.143 1.257 -2.348 2.456 -3.61 3.593 c -0.61 0.55 -0.078 2.108 0.027 2.908 c 0.161 1.225 0.321 2.448 0.482 3.672 C 248.551 158.066 248.918 160.863 249.285 163.66 M 247.862 173.869 c -6.097 1.471 -11.016 8.388 -9.047 14.643 c 0.432 1.372 1.204 2.612 2.179 3.664 c 0.495 0.534 1.041 1.021 1.617 1.467 c 0.653 0.502 1.531 0.854 2.123 1.411 c 0.475 0.448 0.291 1.185 -0.284 1.438 c -0.682 0.301 -1.455 -0.273 -2.032 -0.593 c -1.426 -0.789 -2.723 -1.802 -3.845 -2.981 c -4.498 -4.73 -5.974 -11.945 -3.427 -17.992 c 1.303 -3.092 3.468 -5.831 6.11 -7.891 c 1.219 -0.949 2.553 -1.76 3.98 -2.36 c 0.343 -0.143 0.69 -0.275 1.042 -0.393 c 0.59 -0.197 0.285 -0.625 0.215 -1.167 c -0.157 -1.232 -0.314 -2.464 -0.472 -3.697 c -0.359 -2.815 -0.718 -5.633 -1.078 -8.449 c -9.868 7.855 -21.147 17.782 -20.896 31.557 c 0.227 12.347 11.517 22.026 23.729 21.048 c 1.117 -0.09 2.226 -0.277 3.313 -0.55 c 0.794 -0.2 0.36 -1.004 0.271 -1.705 c -0.089 -0.702 -0.18 -1.404 -0.269 -2.107 c -0.359 -2.812 -0.717 -5.624 -1.077 -8.437 C 249.298 185.139 248.58 179.505 247.862 173.869 M 260.24 111.021 c 0.114 -3.351 -2.348 -7.081 -6.035 -5.687 c -2.881 1.089 -4.752 3.944 -6.029 6.601 c -3.496 7.271 -3.876 15.193 -2.535 23.05 C 252.999 129.292 259.946 120.728 260.24 111.021 M 254.311 202.146 c 6.132 -3.029 10.09 -10 8.94 -16.826 c -1.072 -6.361 -6.155 -11.264 -12.673 -11.62 C 251.819 183.182 253.063 192.664 254.311 202.146',
	ronde : 'M 243.874 5.449 c 7.086 -0.436 20.173 3.449 20.458 14.391 c 0.303 10.697 -13.037 14.808 -19.926 14.409 c -7.293 -0.145 -20.116 -4.779 -20.396 -14.409 C 223.704 10.548 237.07 5.614 243.874 5.449 Z M 246.532 31.844 c 4.23 -0.625 5.878 -2.823 5.584 -9.539 c -0.494 -9.232 -5.453 -15.539 -10.369 -14.397 c -4.128 0.982 -5.868 3.126 -5.573 9.561 C 236.565 26.254 241.612 32.636 246.532 31.844 Z',
	round2 : 'M 0 0 c -5.5 -1.7 -9.85 -6.3 -9.9 -10.4 c 0 -11.7 25.14 -16.34 35.52 -6.54 c 11.2 10.6 -7.1 22.7 -25.7 17 Z M 16.7682 -2.9403 c 3.05854 -4.66792 0.13433 -13.9164 -5.16332 -16.3302 c -7.77978 -3.54471 -12.5485 2.49585 -9.30827 11.7908 c 2.2412 6.42911 11.3594 9.28925 14.4716 4.53937 Z',
	round:'m 153 108 a 11.2 7.3 0 1 1 -0.3,-1.8Z'
		     
}

var log = function(message) {
	console.log(message);
};

var getDot = function(st, x, y) {
	var dot = 81 +2* Math.floor( ( st.options.y-y) / (st.options.space));
	return dot;

};

var moveDot = function(st, dot) {
	var space = st.options.space / 4;
	
	if (st.activeDot) {
		st.activeDot.transform("S0.45,0.45T100," + (238 - dot * space));
	}
	return st.activeDot;
};

var setDots = function(stave, dots) {
	
	var past=0;
	var mes= 0;
	stave.dots = dots;
	var space = stave.options.space / 4;
	var x=50;
	var tick=4;
	var nbtick=0;
	var nbBar=0;
	var start=0;
	var currents= [];
	
	var alldots = stave.paper.set();
	stave.alldots = alldots;
	var min;
	var max;
	var first;
	var last;
	var zarb
	
	for  (dot of dots.score) {
		if( past==0) {
			if (mes%4==0) {
				var bar = addBar(stave,x, nbBar);
			// bar.end=end;
			}
			min=dot;
			first=dot;
			max=dot;
			last=dot;
		}

		if  (dot.event[0].event.noteNumber<min.event[0].event.noteNumber) {
			min=dot;
		} 
		if  (dot.event[0].event.noteNumber>max.event[0].event.noteNumber) {
			max=dot;
		} 
	
		
		nbtick++;
		currents.push(dot);
		var dotn = addDot(stave, x, dot);
		dotn.event=dot.event[0];
		dotn.posx=x;
		past+=dot.event[0].event.t;
		dot.sdot=dotn;
		alldots.push(dotn);
		
		var tabn =addTab(stave,  x,  dot);
		alldots.push(tabn);
		dot.stab=tabn;
		
		x+=40;
		
		
		if (past==384) {
			mes++;
			if (mes%4==0) {
				nbtick=0;
				nbBar++;
				// var bar = addBar(stave,x, nbBar);
			// bar.end=end;
			}
			
		
			last = dot;
			past =0;
			var box =  first.box;
			
			
			// draw previous bars
			if (first.event[0].event.noteNumber==last.event[0].event.noteNumber) {
			
				var yy = min.sdot.box.y2+stave.options.space*3;
				var pp4 = "M"+(first.sdot.box.x+0)+" "+yy+" L"+(last.sdot.box.x+0)+" "+yy;
				var a = stave.paper.path(pp4).attr({"stroke-width": 3});
				alldots.push(a);
				
				var pp5 = "M"+(first.sdot.box.x)+" "+(yy-5)+" L"+(last.sdot.box.x+0)+" "+(yy-5);
				var a5 = stave.paper.path(pp5).attr({"stroke-width": 3});
				alldots.push(a5);
				for (current of currents) {
						var box3 =  current.sdot.box;
						var pp3 = "M"+(box3.x+0)+" "+(box3.y2-stave.options.space/2+0)+" L"+(box3.x+0)+" "+yy;
						var a = stave.paper.path(pp3).attr({"stroke-width": 2});
						alldots.push(a);
				}
			}
			else
			if (first.event[0].event.noteNumber>last.event[0].event.noteNumber) {
				if (last==max)  {
					
				}
				else {
				
					var box =  first.sdot.box;
					var box2 =  last.sdot.box;
					
					var pp = "M"+(box.x2+0)+" "+(box.y+stave.options.space/2+0)+" L"+(box.x2+0)+" "+(box.y-stave.options.space*3);
					var a = stave.paper.path(pp).attr({"stroke-width": 2});
					alldots.push(a);
					var pp2 = "M"+(box2.x2+0)+" "+(box2.y+stave.options.space/2+0)+" L"+(box2.x2+0)+" "+(box2.y-stave.options.space*3);
					var a2 = stave.paper.path(pp2).attr({"stroke-width": 2});
					alldots.push(a2);
					
					
					
					var pp3 = "M"+(box.x2+0)+" "+(box.y-stave.options.space*3)+" L"+(box2.x2+0)+" "+(box2.y-stave.options.space*3);
					var a3 = stave.paper.path(pp3).attr({"stroke-width": 3});
					alldots.push(a3);
					
					var pp4 = "M"+(box.x2+0)+" "+(box.y-stave.options.space*3+5)+" L"+(box2.x2+0)+" "+(box2.y-stave.options.space*3+5);
					var a4 = stave.paper.path(pp4).attr({"stroke-width": 3});
					alldots.push(a4);
					/*
					 * var pp4 = "M"+(box.x2+0)+"
					 * "+(box.y+stave.options.space*3+4)+" L"+(box2.x2+0)+"
					 * "+(box2.y+stave.options.space*3+4); var a4 =
					 * stave.paper.path(pp3).attr({"stroke-width": 3});
					 */
					
					// the others
					for (current of currents) {
						if (current!=last && current!=first) {
						
							var box3 =  current.sdot.box;
							var yp = a3.getPointAtLength(box3.x2).y;
							var pp3 = "M"+(box3.x2+0)+" "+(box3.y2-stave.options.space/2+0)+" L"+(box3.x2+0)+" "+(yp);
							var a = stave.paper.path(pp3).attr({"stroke-width": 2});
							
							alldots.push(a);
							
							
						}
					}
				}
			}	
			else {
				var box =  first.sdot.box;
				var box2 =  last.sdot.box;
				if (first==min)  {
				
					
					var pp = "M"+(box.x+0)+" "+(box.y2-stave.options.space/2+0)+" L"+(box.x+0)+" "+(box.y2+stave.options.space*3);
					var a = stave.paper.path(pp).attr({"stroke-width": 2});
					alldots.push(a);
					var pp2 = "M"+(box2.x+0)+" "+(box2.y2-stave.options.space/2+0)+" L"+(box2.x+0)+" "+(box2.y2+stave.options.space*3);
					var a2 = stave.paper.path(pp2).attr({"stroke-width": 2});
					alldots.push(a2);
					var pp3 = "M"+(box.x+0)+" "+(box.y2+stave.options.space*3)+" L"+(box2.x+0)+" "+(box2.y2+stave.options.space*3);
					var a3 = stave.paper.path(pp3).attr({"stroke-width": 3});
					alldots.push(a3);
				
					var pp4 = "M"+(box.x+0)+" "+(box.y2+stave.options.space*3-5)+" L"+(box2.x+0)+" "+(box2.y2+stave.options.space*3-5);
					var a4 = stave.paper.path(pp4).attr({"stroke-width": 3});
					alldots.push(a4);
				
					
					// the others
					for (current of currents) {
						if (current!=last && current!=first) {
						
							var box3 =  current.sdot.box;
							
							var yp = a3.getPointAtLength(box3.x).y;
							var pp3 = "M"+(box3.x+0)+" "+(box3.y2-stave.options.space/2+0)+" L"+(box3.x+0)+" "+(yp+stave.options.space/2);
							var a = stave.paper.path(pp3).attr({"stroke-width": 2});
							alldots.push(a);

							
							
						}
					}
					
				}
				else {
				
					var yy = min.sdot.box.y2+stave.options.space*3;
					var pp4 = "M"+(first.sdot.box.x+0)+" "+yy+" L"+(last.sdot.box.x+0)+" "+yy;
					var a = stave.paper.path(pp4).attr({"stroke-width": 3});
					alldots.push(a);
					
					var pp5 = "M"+(first.sdot.box.x)+" "+(yy-5)+" L"+(last.sdot.box.x+0)+" "+(yy-5);
					var a5 = stave.paper.path(pp5).attr({"stroke-width": 3});
					alldots.push(a5);
					for (current of currents) {
							var box3 =  current.sdot.box;
							var pp3 = "M"+(box3.x+0)+" "+(box3.y2-stave.options.space/2+0)+" L"+(box3.x+0)+" "+yy;
							var a = stave.paper.path(pp3).attr({"stroke-width": 2});
							alldots.push(a);
					}
					
					
				}
			}
			currents=[];
		}
		
		
	}

};


var addBar = function(stave,  x, n) {
	
	// stave.paper.circle(100, dot * space - 320, space);
	if (n!=0) {
	var barString = "M"+(x-10) +" "+st.startBar+"L"+(x-10) +" "+st.endBar;
	
	bar = stave.paper.path(barString);
	}
	var number = stave.paper.text(x,  st.startBar-65, n+1).attr({"font-family":"times",  "font-weight":"bold", "font-size":"16"});
	return number
};

var addDot = function(stave, x, dot) {
	var st = stave.paper.set();
	
	
	var space = stave.options.space / 4;
	
	// stave.paper.circle(100, dot * space - 320, space);
	
	// var round = stave.paper.path(symbols.round);
	var sdot = dotToScale(dot.event[0].event.noteNumber);
	dot.event[0].event.scale=sdot;
	var l = 15+stave.options.start - sdot.score* space*2+(6-sdot.octave)*14*space;
	var xstart = x-15;
	
	if (sdot.sharp) {
	
		var sharp1= stave.paper.path("M"+(xstart) +" "+(l-2)+","+(xstart+10)+" "+(l-6));
		sharp1.attr({"stroke-width": 4});
		
		st.push(sharp1);
		
		var sharp2= stave.paper.path("M"+(xstart) +" "+(l+8)+","+(xstart+10)+" "+(l+4));
		sharp2.attr({"stroke-width":4});
		st.push(sharp2);
		
		var sharp3= stave.paper.path("M"+(xstart+2) +" "+(l+12)+","+(xstart+2)+" "+(l-10));
		st.push(sharp3);
		var sharp4= stave.paper.path("M"+(xstart+8) +" "+(l-12)+","+(xstart+8)+" "+(l+10));
		st.push(sharp4);
	}
	// var sharp4= stave.paper.path("M"+(x-6) +" "+(l-10)+","+(x-6)+" "+(l+10));
	
	var round = stave.paper.ellipse(x+5,l,7,5);
	st.push(round);
	
	
		 var i = l;
		 while (i<stave.options.start-5) {
			 
				var trait = stave.paper.path("M"+(x-7)+" "+(i)+","+(x+16)+" "+(i));
				trait.attr({"stroke-width":1});
				st.push(trait);
			 i+=10;
		 }
		 
		 i=l;
		 while (i>stave.options.start+stave.options.space*4) {
			 	var trait = stave.paper.path("M"+(x-7)+" "+(i)+","+(x+16)+" "+(i));
				trait.attr({"stroke-width":1});
				st.push(trait);
			 i-=10;
		 }
		 

	st.box =round.getBBox();
	round.attr("fill", "black");
	if (dot.event[0].event.t<192) {
		round.attr("stroke", "black");
	}
	else {
		round.attr("fill", "white");
	}
	round.transform("R-40");
	// space));
	// matrix(1.2477873,-0.63522684,0.59622185,1.3694102,-142.33379,929.29558)"
	
	
	
	// round.transform("S0.50,0.40t"+x+","+(-965+stave.options.start-
	// sdot.score* space*4)+"
	// m1.2477873,-0.63522684,0.59622185,1.3694102,-142.33379,929.29558");
	// round.transform(""+x+"," + (205+stave.options.start- dot * space));
	// round.transform("S0.45,0.40T"+x+"," + (205+stave.options.start- dot *
	// space));
	round.data("type", "dot");
	round.data("event",event);
	return st;
};

var dotToScale=function(dot) {
	var a = dot%12;
	var octave=Math.floor(dot/12);
	switch(a) {	
		case 0:return { dot:dot, name:"C", fullname:"C#", sharp:false,number:a,score: 0, octave:octave};// C
		case 1:return { dot:dot, name:"C", fullname:"C#", sharp:true,number:a,score: 0, octave:octave};// C#
		case 2:return { dot:dot, name:"D", fullname:"D", sharp:false,number:a,score: 1, octave:octave};// D
		case 3:return { dot:dot, name:"D", fullname:"D#", sharp:true,number:a,score: 1, octave:octave};// D#
		case 4:return { dot:dot, name:"E", fullname:"E", sharp:false,number:a,score: 2, octave:octave};// E
		case 5:return { dot:dot, name:"F", fullname:"F", sharp:false,number:a,score: 3, octave:octave};// F
		case 6:return { dot:dot, name:"F", fullname:"F#", sharp:true,number:a,score: 3, octave:octave};// F#
		case 7:return { dot:dot, name:"G", fullname:"G", sharp:false,number:a,score: 4, octave:octave};// G
		case 8:return { dot:dot, name:"G", fullname:"G#", sharp:true,number:a,score: 4, octave:octave};// G#
		case 9:return { dot:dot, name:"A", fullname:"A#", sharp:false,number:a,score: 5, octave:octave};// A
		case 10:return { dot:dot, name:"A", fullname:"A#", sharp:true,number:a,score: 5, octave:octave};// A#
		case 11:return { dot:dot, name:"B", fullname:"B", sharp:false,number:a,score: 6, octave:octave};// B
	}
	
}


var addTab = function(stave, x, dot) {
	var space = stave.options.space / 4+stave.options.space*dot.event[0].event.s+80+stave.options.start;
	var tab = stave.paper.text(x,  space, dot.event[0].event.f+"").attr({"font-family":"times",  "font-weight":"bold", "font-size":"16"});
	tab.data("note", dot);
	tab.data("type", "tab");
	tab.data("event",dot.event[0].event);
	return tab;
};


var activeDot = null;

var getStringAt=function(event, stave) {
	
	var top = document.getElementById("score").offsetTop;
	var firstString=stave.options.start+9*stave.options.space;
	var string = Math.round((event.layerY -firstString)/stave.options.space);
	return string;
};
var tempElement;

var drawStave = function(options) {

	// stave.paper.circle(100, dot * space - 320, space);

	var x = options.x;
	var y = options.y;
	var paper = Raphael("score", options.width, options.height);
	
/*
 * var path = paper.path ("M0 50 L10000 10"); var pp= 1000;
 * 
 * var path2 = paper.circle (path.getPointAtLength(pp).x,
 * path.getPointAtLength(pp).y, 5); var path2 = paper.circle (0,
 * path.getPointAtLength(pp).y, 5); var path2 = paper.circle
 * (path.getPointAtLength(pp).x,0, 5);
 */
	// var paper = Raphael(options.x, options.y, options.width, options.height);
	var space = 0;
	var st = paper.set();
	var line = 0;
	var space = 0
	var start = options.start;
	var nnbar=null;

	for (; line < options.line; line++, space += options.space) {
		var path = "M" + x + " " + (start + space) + "L" + (x + options.width)
				+ " " + (start + space);
		var dline = paper.path(path);
	
		if (line == 3) {
			st.central = (start + space) + y;
		}
		st.push(dline);
	}

	space += 4 * options.space;
	line = 0;
	for (; line < 6; line++, space += options.space) {
		var path = "M" + x + " " + (start + space) + "L" + (x + options.width)
				+ " " + (start + space);
		var dline = paper.path(path);
	
		st.push(dline);
	}
	
	st.startBar=start;
	st.endBar=start+space-options.space;
	
	var bar = "M"+(x + options.width)+" "+st.startBar+"L"+(x + options.width)+" "+st.endBar;
	 paper.path(bar);
	 st.attr("stroke", '#000000');
	 st.attr("stroke-width", "1");
     st.translate(0.5, 0.5);// dline.glow({color: '#f00',width: 40});
	 st.attr("opacity", 1);

	// Treble
	var treble = paper.path(symbols.treble);
	treble.attr("fill", '#000000')
	treble.transform("s0.5,0.5t-450,-" + (90 -options.start+ start));

	// T.A.B
	paper.text(20, 100+options.start, "T").attr(
			  {"font-family":"arial",
			   "font-weight":"bold",
			   "font-size":"20"});
	
	paper.text(20, 115+options.start, "A").attr(
			  {"font-family":"arial",
			   "font-weight":"bold",
			   "font-size":"20"});
	
	paper.text(20, 130+options.start, "B").attr(
			  {"font-family":"arial",
			   "font-weight":"bold",
			   "font-size":"20"});
	
	paper.canvas.addEventListener('mouseup', function(event) {
		nbinit=null;
	
		
	});
	
	paper.canvas.addEventListener('mousedown', function(event) {
		var xx = document.getElementById("score").scrollLeft;
		nbinit = event.clientX+xx;
		
	});
	
	
	paper.canvas.addEventListener('click', function(event) {
		var x = event.clientX;
		var y = event.clientY;
		nbinit=null;
	
		var xx = document.getElementById("score").scrollLeft;
		var hover= paper.getElementByPoint(x, y);
		if (hover==null) {
			var found= null;
			var stop=false
			var i =0;
			
			st.alldots.forEach(function(e){
			if (stop) {
				return;
			}
				 found=e;
				 if (e.posx && e.posx>x+xx) {
					stop=true;
				}
			});
		
			 player.currentTime = found.event.event.queuedTime; 
			 setRepere(found.posx);
		}
	
	}
	);
	
	// ondragstart - fires when the user starts to drag an element
	// ondrag - fires when an element is being dragged
	// ondragend - fires when the user has finished dragging the element
	
	var nbinit=null;
	var selectDot = null;
	paper.canvas.addEventListener('mousemove', function(event) {

		var top = document.getElementById("score").offsetTop;
		var x = event.clientX;
		var y = event.clientY; 
		// log(x+" "+y+" "+getStringAt(event, st));
		// var dot = getDot(st, event);
		var hover= paper.getElementByPoint(x, y);
			
		// log(event.x+' '+document.getElementById("score").scrollLeft+"
		// "+document.getElementById("score").offsetLeft);
		if (hover==null || hover==selectDot) {
			if (nnbar) {
				nnbar.remove();
			}
			if (hoverElement==null ){
				
				var xx = document.getElementById("score").scrollLeft;
				
				
				
				if (event.buttons==1) {
					log(nbinit+" "+(x+xx)+" "+Math.abs(nbinit-event.x));
					if (selectDot) {
						selectDot.remove();
					}
				
					if (nbinit) {
						 selectDot =  st.paper.rect(Math.min(nbinit, x+xx), 0,Math.abs(nbinit-(x+xx)), 300).attr("fill", 'blue').attr("opacity",0.1);
						 selectDot.toBack();
					}
				
					
					return;
				}
				
				
			}
			
		 
			nnbar = st.paper.path("M"+xx+" 0 L"+xx+" 300");
			
			return;
		}
		if (nnbar) { 
			nnbar.remove();
			nnbar = null;
		}
		
		if(hover!=null && hover.data("drag")!="on") {
			hover.data("drag", "on");
			hover.drag(
				function(dx, dy, x, y, event) {
					// move drag
				
					if (hoverElement.data("type")=='tab') {	
						hoverElement.data("dx", dx);
						hoverElement.data("dy", dy);
						hoverElement.attr("fill", "#886611")
					
						var string = getStringAt(event, st);
						if (string>0 && string <=6) {
							var space = st.options.space / 4+st.options.space*string+80+st.options.start;
							tempElement.attr("y", space);
							var oldFrette= tempElement.data("event").f;
							var oldString= tempElement.data("event").s;
					
							var newFrette= strings[6-string]- strings[6-tempElement.data("event").s]+oldFrette
							if (oldFrette!=newFrette) {
								tempElement.attr("text", newFrette);
								tempElement.data("event").f= newFrette;
								tempElement.data("event").s= string;
							}
						}
					}
			}, function(event) {
				// start drag
				hoverElement=hover;
			
			
				hoverElement.hide();
				if (tempElement==null) {
				 tempElement = hoverElement.clone();
				 tempElement.data("type", hoverElement.data("type"));
				 tempElement.data("event", hoverElement.data("event"));
				}
				startTimerDrag();	
				// alert(dot);
				}, function(event) {
					// stop drag
			
					if (hoverElement!=null) {
				
					hoverElement.data("drag", "off");
					hoverElement.undrag();	
					if (tempElement !=null) {
						if (tempElement.data("type")=='tab') {
							if (tempElement !=null) {
							if (tempElement.data("type")=='tab') {
								var newDot = tempElement.data("event").f+ strings[tempElement.data("event").s-1];
								hoverElement.data("note").event[0].event.noteNumber=newDot;
								// todo : utile ?
								hoverElement.data("note").event[0].event.s=tempElement.data("event").s;
								hoverElement.data("note").event[0].event.f=tempElement.data("event").f;
								
								// hoverElement.data("event");
								tempElement.remove();
								// now clear all dots
								st.alldots.remove();
								setDots(st, st.dots );
							}
						}
							
						}
						
						
					}
					
					tempElement=null;
					hoverElement=null;
					
					
					}
				}
			);
		
		}
  
	
		// moveDot(st, getDot(st, event));
		var target = event.target;

	});
	
	
	
	
	paper.canvas.addEventListener('mouseleave', function(event) {
		var x = event.clientX;
		var y = event.clientY;
		var target = event.target;
		st.attr("stroke", '#000000');
	
	});

	/*
	 * paper.canvas.addEventListener('mouseleave', function(event) { var x =
	 * event.clientX; var y = event.clientY; var target = event.target;
	 * st.attr("stroke", '#000000');
	 * 
	 * });
	 */

	paper.canvas.addEventListener('mouseenter', function(event) {
		var x = event.clientX;
		var y = event.clientY;
		var target = event.target;
		st.attr("stroke", '#FF0000');

	});
	st.paper = paper;
	st.options = options;
	// st.activeDot = addDot(st, 67, 100);
	return st;
	// paper.renderfix();
};
var callbackDrag=function() {
	
	if(tempElement==null || hoverElement.data("drag")!='on') {
		return;
	}
	if (hoverElement.data("type")=='tab') {
		var dx= hoverElement.data("dx");
		var dy= hoverElement.data("dy");
		
		if (dx>10){
			var newVal = 1+parseInt(tempElement.data("event").f);
			tempElement.attr('text',newVal ,10);
			tempElement.data('event').f=newVal;
		}
		if( dx<-10) {
			var newVal =parseInt(tempElement.data("event").f)-1;
			if (newVal>0) {
				tempElement.attr('text', newVal);
				tempElement.data('event').f=newVal;
			}
		}
	
		startTimerDrag();
		return;
	}
	
};



var startTimerDrag= function() {

	if (hoverElement!=null) {
		setTimeout("callbackDrag();", 400);
	}
	
};

var st = drawStave(options);
var score =[ { n:67, s:3, f:0}, {n:69, s:3, f:2}, {n:71, s:2, f:0}, {n:73, s:2, f:1}, {n:71, s:2, f:0}, {n:69, s:3, f:2},{ n:67, s:3, f:0 } ];



var getTab = function(dot) {
	i=0;
	for ( string of strings) {
		i++;
		if (dot>=string) {
			return { string:i, frette:dot-string};
		}
	}
	
	return 
};

var strings= [ 64, 59, 54, 50, 45, 40 ];

var creatreScoreFromEvents = function(events) {
	var score= { event:events, score: []};
	var time=0;
	var queuedTime=0;
	var last = null;
	var id=0;
	var ide=0;
	for (event of events) {
		queuedTime += event[1];
		if (event[0].event.subtype=='noteOn')  {
			if (!event[0].event.s) {
			var found = getTab(event[0].event.noteNumber);
			event[0].event.s=found.string;
			event[0].event.f=found.frette;
			}
			
			event[0].event.score=id;
			event[0].event.ide=ide;
			last =  { event:event, time:queuedTime };
			score.score.push(last);
			event[0].event.queuedTime =queuedTime;
			
			id++;		
		}
		if (event[0].event.subtype=='noteOff')  {
			var duree = event[0].ticksToEvent;
			last.event[0].event.t = duree;
			last.event[0].event.tick = event[1];
			log("=>"+event[1]);
		}
		ide++;
	}
	return score;
}
// setDots(st, score);

var savePlayer=function() {
	var data = player.replayer.getData();
	
	// / comp = LZW.compress(string),
	  // decomp = LZW.decompress(comp);
	 // alert(comp.length);
	
	
	
	ajaxJSONPost("save", data, function () {
		alert("back");
	});
};


var loadPlayer=function() {
	ajaxJSONGet("load",  function (data) {
		
		player.loadGDemos(data);
		setRepere(20);
		
		if (st.alldots) {
		st.alldots.remove();
		}
		var dots = creatreScoreFromEvents(data);
		setDots(st, dots);
		setSynthesia(dots);
		
	});
};

function ajaxJSONGet(url, callback){
	var http_request = new XMLHttpRequest();
	http_request.open("GET", url, true);
	http_request.onreadystatechange = function () {
	  var done = 4;
	  var ok = 200;
	  if (http_request.readyState === done && http_request.status === ok){
	callback(JSON.parse(http_request.responseText));
	  }
	};
	http_request.send();
	}
	 
	function ajaxJSONPost(url, jsondata, callback){
	var xhr = new XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onreadystatechange = function () {
	   if (xhr.readyState == 4 && xhr.status == 200) {
	       callback(xhr.responseText);
	   }
	}
	
	var string =JSON.stringify(jsondata);
	var data =JSON.stringify(LZW.compress(string));
	alert(string.length+" "+data.length)
	xhr.send(string);
}
	
	
	
	var isOscillator = true;
	var isSoundFont = true;
	var oscillator = function() {
		isSoundFont = !isSoundFont;
	};
	var soundfont= function() {
		isOscillator = !isOscillator;
	};
	
	
	
	// http://rosettacode.org/wiki/LZW_compression#JavaScript
	// LZW Compression/Decompression for Strings
	var LZW = {
	    compress: function (uncompressed) {
	        "use strict";
	        // Build the dictionary.
	        var i,
	            dictionary = {},
	            c,
	            wc,
	            w = "",
	            result = [],
	            dictSize = 256;
	        for (i = 0; i < 256; i += 1) {
	            dictionary[String.fromCharCode(i)] = i;
	        }
	 
	        for (i = 0; i < uncompressed.length; i += 1) {
	            c = uncompressed.charAt(i);
	            wc = w + c;
	            // Do not use dictionary[wc] because javascript arrays
	            // will return values for array['pop'], array['push'] etc
	           // if (dictionary[wc]) {
	            if (dictionary.hasOwnProperty(wc)) {
	                w = wc;
	            } else {
	                result.push(dictionary[w]);
	                // Add wc to the dictionary.
	                dictionary[wc] = dictSize++;
	                w = String(c);
	            }
	        }
	 
	        // Output the code for w.
	        if (w !== "") {
	            result.push(dictionary[w]);
	        }
	        return result;
	    },
	 
	 
	    decompress: function (compressed) {
	        "use strict";
	        // Build the dictionary.
	        var i,
	            dictionary = [],
	            w,
	            result,
	            k,
	            entry = "",
	            dictSize = 256;
	        for (i = 0; i < 256; i += 1) {
	            dictionary[i] = String.fromCharCode(i);
	        }
	 
	        w = String.fromCharCode(compressed[0]);
	        result = w;
	        for (i = 1; i < compressed.length; i += 1) {
	            k = compressed[i];
	            if (dictionary[k]) {
	                entry = dictionary[k];
	            } else {
	                if (k === dictSize) {
	                    entry = w + w.charAt(0);
	                } else {
	                    return null;
	                }
	            }
	 
	            result += entry;
	 
	            // Add w+entry[0] to the dictionary.
	            dictionary[dictSize++] = w + entry.charAt(0);
	 
	            w = entry;
	        }
	        return result;
	    }
	}// For Test Purposes

	