
var synOn = function(dot)  {
	var sdot = dotToScale(dot);
	var color = getColorPiano(sdot.score);
	allSyn[dot-33].attr("fill", color);
	
	
	
};

var synOff= function(dot) {
	var on = allSyn[dot-33];
	if (on.data("black")) {
		on.attr("fill", '#000000');
	}
	else {
		on.attr("fill", '#ffffff');
	}

};
var echelle=1;
var setSynthTime =function(time) {
	
	var dt =  (player.endTime-time*1000)/echelle;
 	synthesiaVal.setViewBox(0, (player.endTime-time*1000-500), 800, 500, false);
} ;




var getColorPiano =function(dot){
	switch(dot) {
	 	case 0:return  'green'
	 	case 1:return 'magenta';
	 	case 2: return 'cyan';
	 	case 3: return'purple';
	 	case 4: return'blue';
	 	case 5: return'yellow';
	 	case 6:return 'orange';
	 	default:return 'white';
	}
	
};


var allSyn = [];
var synthesiaVal=null;
var setSynthesia = function(dots)  {
	var w = 800;
	synthesiaVal = Raphael("synthesia2", w, 500);

	
	var end = player.endTime/echelle;
	var lpiano= w/52;
	 	
	// synthesiaVal.setViewBox(0, 250, 800, 500, false);
	for (dot of dots.event) {
		if (dot[0].event.subtype=='noteOff') {
			// log(dot[0].event);
		}
		if (dot[0].event.subtype=='noteOn') {
			var x =  ((dot[0].event.scale.score-5)+7*(dot[0].event.scale.octave-2))*lpiano;
			var past = dot[0].event.queuedTime;
			var len = dot[0].event.tick;
		 	if(dot[0].event.scale.sharp) {
		 		var elt=synthesiaVal.rect(x+lpiano*3/4,(end-past-len)/echelle,lpiano/2,len/echelle);
			 
		 		
			 	elt.attr("fill", '#f00');	
		 	}else {
			var elt=	synthesiaVal.rect(x,(end-past-len)/echelle,lpiano,len/echelle);
			var color = getColorPiano(dot[0].event.scale.score);
			elt.attr("fill",color);
		 	
			
		 	
		 	}
		 }	
	}
};

var synthesia =function() {
	var w = 800;
	var h = 70;
	var h2=300;
	
	// synthesiaVal = Raphael("synthesia2", w, 500);
	var paper = Raphael("synthesia", w,h);
	
	// 
	var lpiano= w/52;
	var tpiano=0;
	var xpiano=0;
	var xpre=0;
	var ww=60;
	var ypiano=h-ww;
	while(tpiano<51) {
		
		var st = paper.set();
		var black = paper.rect(tpiano*lpiano, ypiano, lpiano, ww-1)
		black.data("black", false);
		st.push(black);
		allSyn.push(black);
		switch (tpiano%7) {
		case 0:
		case 2:
		case 3:
		case 5:
		case 6:
			var black = paper.rect(tpiano*lpiano+lpiano*3/4, ypiano, lpiano/2, ww*2/3);
			black.attr("fill", '#000000');
			black.data("black", true);
			st.push(black);
			allSyn.push(black);
			break;
		}
		
		tpiano++;
	}
	for (syn of allSyn) {
		if (syn.data("black")) {
			syn.toFront();
		}
	}
		
	
	
} ;
